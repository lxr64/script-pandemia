#!/bin/bash
rpcport=47668
port=47666
export coin=pandemia

USERHOME=`eval echo "~$USER"`

echo  "


                         __    __
                    _wr'''       ''-q_
                 _dP                 9m_
               _#P                     9#_
              d#@                       9#m
             d##                         ###
            J###                         ###L
            {###K                       J###K
            ]####K      ___aaa___      J####F
        __gmM######_  w#P   9#m  _d#####Mmw__
     _g##############mZ_         __g##############m_
   _d####M@PPPP@@M#######Mmp gm#########@@PP9@M####m_
  a###          ,Z #####@    @####\g             M##m
 J#@             0L   *##     ##@  J#               *#K
 #                #     _PANDM_~    dF               '#_
7F                 #_   ]#####F   _dK                 JE
]                    *m__ ##### __g@'                   F
                       'PJ#####LP'
 '                       #######_                      '
                       _0########_
     .               _d#####^#####m__              ,
      '*w_________am#####P'   ~9#####mw_________w*'
          ''9@#####@M''           ''P@#####@M''


                      Pandemia "



echo ""
echo This script must be executed over a success installation of MN in your VPS,
echo it will copy the files and folders and will make all necesary changes for create multiples
echo Master Nodes for $money, please becarefull that script could overwrite your wallet information
echo if you run for second time, please always backup yours wallet.dat files before run it.
echo ""

echo "Do you have installed your first Pandemia MN in this VPS ? [Y/n] :  "

while :
do
  read INPUT_STRING
  case $INPUT_STRING in
	y)
		break
                ;;
	n)
		break
		;;
        *)
                INPUT_STRING=y
                break
                ;;
  esac
done

if [ $INPUT_STRING == "y" ]
then
    read -p "Please enter the cuantity or range of additionals MasterNodes do you want? [(xx or xx-xx) default 1] :" cant
    read -p "Enter de instalation directory [${USERHOME}/.$coin] :" dir

    echo $dir $cant


    if [ -z $dir ]
      then
      dir="${USERHOME}/.$coin"
    fi

    if [ -z $cant ]
     then
       inicio=1
       fin=1
     else
       inicio="$(cut -d'-' -f1 <<< $cant)"
       fin="$(cut -d'-' -f2 <<< $cant)"
    fi

    if [ -d $dir ]
      then

      echo
      echo Creating Directories
      echo

      for ((x=$inicio;x<=$fin;x++))
      do
        echo "cp -rp "$dir" "$dir"_"$x"/"|sh
        echo "cp -p "$dir"_"$x"/wallet.dat "$dir"_"$x"/wallet_multimnbak.dat "|sh
        echo "rm -rf "$dir"_"$x"/wallet.dat"|sh
        echo "rm -rf "$dir"_"$x"/"$coin".conf"|sh
      done

      echo
      echo Get new Masternodes Keys
      echo

      ps -fea|grep -s "."$coin"_" |grep -v "grep"| awk '{ print "kill -9 "$2 }'|sh

      for ((x=$inicio;x<=$fin;x++))
      do
        newport=$((rpcport + x))

        RPCUSER=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 12 | head -n 1)
        RPCPASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

        mnpk=$(echo ""|awk '{"$coin-cli masternode genkey"|getline ; print $0}')

        echo ""
        echo Master Node Private Key:
        echo $mnpk

	cat $dir"/"$coin".conf"|awk '{if(/listen=/ || /masternodeprivkey=/ || /rpcuser=/ || /rpcpassword=/ || /rpcport=/ )  { print "" } else {print $0}}' > $dir"_"$x"/"$coin".conf"
	echo "masternodeprivkey="$mnpk >> $dir"_"$x"/"$coin".conf"
	echo "rpcuser="$RPCUSER >> $dir"_"$x"/$coin.conf"
	echo "rpcpassword="$RPCPASSWORD >> $dir"_"$x"/"$coin".conf"
	echo "listen=0" >> $dir"_"$x"/"$coin".conf"
        echo "rpcport="$newport >> $dir"_"$x"/"$coin".conf"


        echo "# Masternode config file" > $dir"_"$x"/masternode.conf"
        echo "# Format: alias IP:port masternodeprivkey collateral_output_txid collateral_output_index" >> $dir"_"$x"/masternode.conf"
        echo "mn1 127.0.0.2:$port $mnpk 0 0" >> $dir"_"$x"/masternode.conf"

      done


      echo
      echo Creating new Wallets save thats address
      echo

      for ((x=$inicio;x<=$fin;x++))
      do
        newport=$((rpcport + x))
        echo $coin"d -datadir="$dir"_"$x"  -conf="$dir"_"$x"/"$coin".conf  -pid="$dir"_"$x"/"$coin".pid -reindex"|sh
        sleep 30
        echo "Wallet "$x" : "
        echo $coin"-cli -datadir="$dir"_"$x"  -conf="$dir"_"$x"/"$coin".conf  -pid="$dir"_"$x"/"$coin".pid getaccountaddress mn1"|sh
        echo ""
      done
    fi

      echo
      echo Now deposit the collaterals in new wallets and edit the masternodes.conf files updating the TXID
      echo and restart the services.
      echo
      echo Enjoy.
      echo
      echo Example of restart service
      echo
      echo STOP
      echo $coin"-cli -conf=/root/."$coin"_1/"$coin".conf -datadir=/root/."$coin"_1/ -pid=/root/."$coin_1"/"$coin".pid stop"
      echo
      echo START
      echo $coin"d -conf=/root/."$coin"_1/"$coin".conf -datadir=/root/."$coin"_1/ -pid=/root/."$coin"_1/"$coin".pid"

fi
